const cote = require('cote');
const mysql = require('mysql2/promise');
const config = require('config');

const UserService = require('./services/UserService');

const userResponderService = new cote.Responder({
    name: 'User Service',
    key: 'user',
    respondsTo: ['authenticate', 'logout', 'registration']
});
let dbConnection, userService;

const init = async () => {
    const tryDbConnection = timeout => {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                mysql.createConnection({
                    host: config.database.hostname,
                    user: config.database.user,
                    password: config.database.password || '',
                    port: config.database.port,
                    database: config.database.dbName
                })
                    .then(dbConnection => {
                        console.log('Established database connection.');
                        resolve(dbConnection);
                    })
                    .catch(err => {
                        console.error('Could not establish database connection.', err);
                        reject(err);
                    });
            }, timeout);
        });
    };
    const awaitDbConnection = async () => {
        let iteration = 0;
        let connection;
        while (!connection && iteration < 30) {
            try {
                connection = await tryDbConnection(iteration === 0 ? 200 : 1000);
            } catch (err) {
                iteration++;
            }
        }
        return connection;
    };
    try {
        dbConnection = await awaitDbConnection();
    } catch (err) {
        console.error(`Database Connection could not be established: ${err.message}; kill process`);
        process.exit(1);
    }
    userService = new UserService(dbConnection);
};

userResponderService.on('authenticate', async (req, cb) => {
    try {
        const result = {
            found: false // results in "401 Unauthorized" in middleware
        };
        const { emailAddress, password } = req.query;
        const dbEntry = await userService.getUserByEmailAndPassword(emailAddress, password);
        if (dbEntry) {
            const dbToken = await userService.createAuthTokenForUser(dbEntry.id);
            result.found = true;
            result.data = {
                authToken: dbToken.authToken,
                userId: dbEntry.id,
                tokenValidUntil: dbToken.validUntil
            };
        }
        cb(null, result);
    } catch (err) {
        console.error('Error occurred during authenticate', err);
        cb(err); // results in "401 Unauthorized" in middleware
    }
});

userResponderService.on('register', req => userService.register(req.query));

userResponderService.on('logout', (req, cb) => {
    console.log('logout > req', req);
    // TODO: Use Publisher to broadcast logout (middleware can clear cache)
    cb(null, true);
});

init();