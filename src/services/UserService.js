const {createHash} = require('crypto');
const uuid = require('uuid').v4;
const moment = require('moment');
const {isValidEmail, isValidPw} = require('../lib/validation');

class UserService {
    constructor(dbConnection) {
        this.dbConnection = dbConnection;
    }

    async getUserByEmailAndPassword(emailAddress, password) {
        const hashSha256 = createHash('sha256');
        hashSha256.update(password);
        const sqlStatement = 'SELECT * FROM `users` WHERE `emailAddress` = ? AND `passwordHash` = ?';
        const sqlValues = [
            String(emailAddress).toLowerCase(),
            hashSha256.digest('hex')
        ];
        const [[entry]] = await this.dbConnection.execute(sqlStatement, sqlValues);
        return entry || null;
    }

    async createAuthTokenForUser(userId) {
        const authToken = uuid();
        const tokenValidDt = moment().add(7, 'days');

        const sqlStatement = 'INSERT INTO `tokens` VALUES (?, ?, ?)';
        const [createdToken] = await this.dbConnection.execute(sqlStatement, [
            authToken,
            tokenValidDt.format('YYYY-MM-DD HH:mm:ss'),
            userId,
        ]);
        return createdToken && createdToken.affectedRows ? {
            authToken,
            validUntil: tokenValidDt,
        } : null;
    }

    async register({ firstName, lastName, emailAddress, password }) {
        const result = { success: false };
        try {

            const checkResult = await this._checkForRegistration(emailAddress, password);
            if (checkResult && checkResult.errKey) {
                return Promise.resolve({ ...result, ...checkResult });
            }

            const registerResult = await this._createUser(firstName, lastName, emailAddress, password);
            if (!registerResult) {
                return Promise.resolve({ ...result, errKey: 'error_persisting' });
            }

            const dbEntry = await this.getUserByEmailAndPassword(emailAddress, password);
            if (dbEntry) {
                const dbTokenEntry = await this.createAuthTokenForUser(dbEntry.id);
                result.success = true; 
                result.data = {
                    authToken: dbTokenEntry.authToken,
                    userId: dbEntry.id,
                    tokenValidUntil: dbTokenEntry.validUntil
                };
            }

            return Promise.resolve(result);
        } catch(err) {
            console.error('Error occurred during authenticate', err);
            return Promise.reject({ ...result, errKey: 'internal_error' });
        }
    }

    async _checkForRegistration(emailAddress, password) {
        if (!isValidEmail(emailAddress)) {
            return {
                errKey: 'invalid_emailaddress'
            };
        }
        if (!isValidPw(password)) {
            return {
                errKey: 'invalid_password'
            };
        }

        const sqlStatement = 'SELECT * FROM `users` WHERE `emailAddress` = ?';
        const [[foundEmail]] = await this.dbConnection.execute(sqlStatement, [String(emailAddress).toLowerCase()]);
        if (foundEmail) {
            return {
                errKey: 'already_exists'
            };
        }

        return;
    }

    async _createUser(firstName, lastName, emailAddress, password) {
        const hashSha256 = createHash('sha256');
        hashSha256.update(password);
        const sqlStatement = 'INSERT INTO `users` (`id`, `firstName`, `lastName`, `emailAddress`, `passwordHash`) VALUES (?, ?, ?, ?, ?)';
        const createdEntry = await this.dbConnection.execute(sqlStatement, [
            uuid(),
            String(firstName || '').trim(),
            String(lastName || '').trim(),
            String(emailAddress).toLowerCase(),
            hashSha256.digest('hex')
        ]);
        return createdEntry ? createdEntry : null;
    }
}

module.exports = UserService;
