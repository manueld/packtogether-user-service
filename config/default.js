const config = {
    database: {
        hostname: 'packtogether-user-db',
        user: 'root',
        port: 3306,
        dbName: 'packtogether_user'
    }
};

module.exports = config;
