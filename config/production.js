const config = {
    database: {
        hostname: 'packtogether-user-db',
        user: process.env['MYSQL_USER'],
        password: process.env['MYSQL_PASSWORD'],
        port: 3306,
        dbName: 'packtogether_user'
    }
};

module.exports = config;
