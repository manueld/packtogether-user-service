CREATE TABLE `packtogether_user`.`users` (
  `id` VARCHAR(36) NOT NULL,
  `firstName` VARCHAR(50) NOT NULL,
  `lastName` VARCHAR(50) NOT NULL,
  `emailAddress` VARCHAR(250) NOT NULL,
  `passwordHash` VARCHAR(250) NOT NULL,
  `lastLoggedIn` DATETIME DEFAULT NULL,
  `createdAt` DATETIME DEFAULT NOW(),
  `updatedAt` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX (`emailAddress`)
);