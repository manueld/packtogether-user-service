CREATE TABLE `packtogether_user`.`tokens` (
  `authToken` VARCHAR(36) NOT NULL,
  `validUntil` DATETIME NOT NULL,
  `userId` VARCHAR(36) NOT NULL,
  PRIMARY KEY (`authToken`)
);
